-- Pertama, buat database baru
CREATE DATABASE challenge_ch3;

-- Buat table users_game_biodata
CREATE TABLE users_game_biodata (
  id BIGSERIAL PRIMARY KEY, 
  name VARCHAR(255) NOT NULL,
  birth_date DATE NOT NULL,
  country VARCHAR(255) NOT NULL,
  url_avatar VARCHAR(255) DEFAULT 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
);

-- Buat tabel users_game
CREATE TABLE users_game (
  id BIGSERIAL PRIMARY KEY,
  users_game_biodata_id BIGINT NOT NULL,
  username VARCHAR(128) NOT NULL,
  password VARCHAR(255) NOT NULL,
  CONSTRAINT fk_users_game_biodata
    FOREIGN KEY(users_game_biodata_id) 
      REFERENCES users_game_biodata(id)
      ON DELETE SET NULL
);

-- Buat tabel users_game_histories
CREATE TABLE users_game_histories (
  id BIGSERIAL PRIMARY KEY,
  users_game_id BIGINT NOT NULL,  
  score BIGINT NOT NULL,
  play_time TIMESTAMPTZ DEFAULT now(),
  CONSTRAINT fk_users_game
    FOREIGN KEY(users_game_id) 
      REFERENCES users_game(id)
      ON DELETE SET NULL
);

-- BAB CRUD

-- CREATE data user game
INSERT INTO users_game_biodata (name, birth_date, country) VALUES ('Muhammad Iqbal', '2001-03-14', 'Indonesia');

INSERT INTO users_game (users_game_biodata_id, username, password) VALUES (1, 'iqbaludinm', '123iQbAl');

INSERT INTO users_game_histories (users_game_id, score) VALUES (1, 30255);

-- READ data users game
SELECT * FROM users_game_biodata;
SELECT * FROM users_game;
SELECT * FROM users_game_histories;

-- UPDATE data user game
UPDATE users_game_biodata 
SET name = 'Muhammad Iqbaludin Zaky' 
WHERE id = 1;

UPDATE users_game 
SET password = 'L4bQ123'
WHERE username = 'iqbaludinm';

UPDATE users_game_histories 
SET score = 45234
WHERE users_game_id = 1;

-- DELETE data user game
-- Dilakukan dari yang hanya memiliki 1 relasi
DELETE FROM users_game_histories WHERE users_game_id = 1;
DELETE FROM users_game WHERE username = 'iqbaludinm';
DELETE FROM users_game_biodata WHERE id = 1;